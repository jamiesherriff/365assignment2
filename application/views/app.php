<!doctype html>
<!-- SENG365 Polls assignment Written by Jamie Sherriff September/October 
2014. -->
<html lang="en" data-ng-app="VoteApp">
    <head>
        <title>
            Sherriff Polls
        </title>
        <meta charset="utf-8">
        <!-- Bootstrap and CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/pollstyles.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <script src="scripts/jquery-1.11.1.min.js"></script>
        <script src="scripts/bootstrap.min.js"></script>       
        <!-- Angular -->
        <script src="scripts/angular.js"></script>
        <script src="scripts/angular-route.js"></script>
        <!-- Charts -->
        <script src="scripts/d3.min.js"></script>
        <script src="scripts/angular-charts.js"></script>
        <!-- My JS files -->
        <script src="js/app.js"></script>
        <script src="js/controllers.js"></script>
    </head>
    <body>

        <div data-ng-controller="NavCtrl">  
            <nav class="navbar navbar-inverse navbar-fixed-top" 
                 role="navigation">
                <div class="main-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li data-ng-class="{active: isActive('/home') }">
                            <a href="#/">Home</a></li>
                        <li data-ng-class="{active: isActive('/polls') }"> 
                            <a href="#/polls">Polls</a></li>
                        <li data-ng-class="{active: isActive('/create') }"> 
                            <a href="#/create">Create Poll</a></li>
                        <li data-ng-class="{active: isActive('/about') }">
                            <a href="#/about">About</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class= "container">                
            <div data-ng-view></div>
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-bottom" 
             role="navigation">
            <div class="footer">
                <p>&copy; <b>Jamie Sherriff 2014</b></p>
            </div>
        </nav>
    </body>
</html>
