<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Polls extends CI_Controller {

    /**
     * Index Page for the polls controller
     * Purpose of this controller is just to load the angular app
     * @category	Controller
     * @author         Jamie Sherriff
     */
    public function index() {
        $this->load->view('app');
    }
}
