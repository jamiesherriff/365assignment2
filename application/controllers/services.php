<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Services Controller
 * This controller defines all the restfull web services
 * It can be accessed directly at the path index.php/service/function_name
 * 
 * @category	Controller
 * @author	Jamie Sherriff
 */
require APPPATH . '/libraries/REST_Controller.php';

class Services extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('poll');
        $this->load->helper('security');
    }

    /**
     * Gets all the corresponding polls at uri segment 3 which is after
     * services/polls, If no pollid is provided then all polls are shown, if an
     * error occurs a 404 response is given.
     */
    function polls_get() {
        $pollID = xss_clean(($this->uri->segment(3)));
        if (!$pollID) {
            if ($pollID === '') {
                $poll = $this->poll->getPolls();
            } else {
                $this->response(array('error' => 'Could not load polls'), 404);
            }
        } else {
            $poll = $this->poll->getPoll($pollID);
        }
        if ($poll) {
            $this->response($poll, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('error' => 'Poll for this id could '
                . 'not be loaded'), 404);
        }
    }

    /**
     * Gets vote counts for a specific poll, returns a 404 if the poll cannot
     * be found
     */
    function votes_get() {
        $voteID = xss_clean(($this->uri->segment(3)));
        if (!$voteID) {
            if ($voteID === '') {
                $this->response(array('error' =>'voteid cannot be empty'), 404);
            }
        } else {
            $vote = $this->poll->getVotesQuestion($voteID);
        }
        if ($vote) {
            $this->response($vote, 200); // 200 being the HTTP response code
        } else {
            $this->response(array('error' => 'Votes could not be loaded'), 404);
        }
    }

    /**
     * Post a vote response in the format services/votes/POLLid/answeriD
     * answerid is just a integer containting the users response for the poll.
     * If succesuful the vote is stored in the database
     * Request will fail if a post body exists since it is not needed. 
     */
    function votes_post() {
        $pollID = xss_clean(($this->uri->segment(3)));
        $answerID = xss_clean(($this->uri->segment(4)));
        $body = xss_clean(file_get_contents('php://input'));

        if ($body) {
            $this->response(array('error' =>'no body content allowed'), 404);
        }
        if (!$pollID || !$answerID) {
            if (!$pollID && $answerID) {
                $this->response(array('error' =>'badly formated poll'), 404);
            }
            if (!$answerID && $pollID) {
                $this->response(array('error' => 'badly formated answer'), 404);
            } else {
                $this->response(array('error' =>
                    'badly formated poll and answer'), 404);
            }
        }

        if ($pollID && $answerID) {
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $vote = $this->poll->updateVote($pollID, $answerID, $ipAddress);

            if (!$vote) {
                $this->response(array('error' => 'Vote is not vaalid'), 404);
            } else {
                $message = array('pollID' => $pollID,
                    'answerID' => $answerID,
                    'IPaddress' => $ipAddress,
                    'message' => 'Vote Recorded!');
                $this->response($message, 200);
            }
        }
    }

    /**
     * Posts a new poll with the information required in the request body,
     * Due to server complications overriding the header the uri location
     * is given in the response body. If a  parameter is missing the request
     * will fail. The new poll will be inserted into the database and will
     * Give a 404 response if this fails
     */
    function polls_post() {
        $question = $this->post('question');
        $title = $this->post('title');
        $answerList = $this->post('answers');
        if ($title && $question && $answerList) {
            $pollID = $this->poll->insertPoll($title, $question, $answerList);
            if ($pollID) {
                $location = site_url('/services/polls');
                $location = $location . '/' . strval($pollID);
                $message = array('title' => $title,
                    'question' => $question,
                    'answers' => $answerList,
                    'location' => $location,
                    'message' => 'ADDED!');
                $this->response($message, 201);
            } else {
                $this->response(array('error' =>
                    'could not insert the poll'), 404);
            }
        } else {
            $this->response(array('error' 
                => 'a required parameter is missing'), 404);
        }
    }

    /**
     * Puts a poll into the database. This allows modification of existing
     * polls. A put request is made at the uri services/polls/[pollid] and the
     * database modifies the poll with that id and if it doesn't exist a 404
     * response is given
     * Has a safelist so three polls cannot modified for marking purposes
     */
    function polls_put() {
        $pollID = xss_clean(($this->uri->segment(3)));
        $safelist = array(1, 2, 7);
        if ($pollID) {
            if (in_array($pollID, $safelist)) {
					   $this->response(array('error' => 'not allowed '
					    .'to modify this poll'), 404);
            } else {

                $data = $this->put();
                $question = $this->put('question');
                $title = $this->put('title');
                $answerList = $this->put('answers');
                $message = array(
                    'id' => $pollID,
                    'title' => $title,
                    'question' => $question,
                    'answers' => $answerList,
                    'message' => 'Modified!');
                if ($message['title'] && $message['question'] 
                        && $message['answers']) {
                    $vote = $this->poll->updatePoll($pollID, $title, 
                            $question, $answerList);
                    if ($vote) {
                        $this->response($message, 200);
                    } else {
                        $this->response(array('error' => 'Poll for this '
                            . 'id could not be modified'), 404);
                    }
                } else {
                    $this->response(array('error' => 'a required parameter '
                        . 'is missing and could not be loaded'), 404);
                }
            }
        } else {
            $this->response(array('error' 
                =>'No poll id provided to modify'), 404);
        }
    }

    /**
     * Deletes a poll by uri segement after services/polls
     * Has a safelist so three polls cannot be deleted for marking purposes
     * Will return a 404
     */
    function polls_delete() {
        //prevent users from deleting the first three polls by id for marking
        $safelist = array(1, 2, 7);
        $pollID = xss_clean(($this->uri->segment(3)));
        if (!$pollID) {
            $this->response('Missing poll', 404);
        } else {
            if (in_array($pollID, $safelist)) {
					   $this->response(array('error' => 'not allowed '.
	   'to modify delete poll'), 404);
            } else {
                $deletedRows = $this->poll->deltePoll($pollID);
                if ($deletedRows) {
                    $message = array('deletedrows' => $deletedRows,
                        'message' => 'poll deleted!');
                    $this->response($message, 200);
                } else {
                    $this->response('Poll could not be deleted', 404);
                }
            }
        }
    }

    /**
     * Deletes all votes for a particular poll will return a 404 response if
     * the database lookup fails.
     */
    function votes_delete() {
        $pollID = xss_clean(($this->uri->segment(3)));

        if (!$pollID) {
            $this->response(array('error' 
                =>'Missing poll'), 404);
        } else {
            $deletedRows = $this->poll->deletePollVotes($pollID);
            if (!$deletedRows) {
                $this->response(array('error' 
                    =>'votes could not be deleted'), 404);
            }
            if ($deletedRows) {
                $message = array('deletedrows' => $deletedRows,
                    'message' => 'votes deleted!');
                $this->response($message, 200);
            }
        }
    }
}
