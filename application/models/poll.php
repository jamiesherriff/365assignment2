<?php

class Poll extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Poll model
     * Purpose of this model is to handle all database related functions
     * for the web services.
     * @category	Model
     * @author         Jamie Sherriff
     */
    
    
    /**
     * Gets a list of all available polls
     * @return all available polls
     */
    public function getPolls() {
        // Gets all rows
        $rows = $this->db->order_by("id", "asc")->get('PollQuestions')->result();  
        if($rows){
            return $rows;
        } else { //assume DB fail
            return false;
        }
    }
    
    /**
     * Gets details for a specific poll
     * @param type $pollid
     * @return boolean or poll information if succesfull
     */
    public function getPoll($pollid) {
        $this->db->select("PollQuestions.*, "
                . "PollQuestions.id AS PollID");
        $this->db->select("PollAnswers.*");
        $this->db->from("PollQuestions");
        $this->db->join("PollAnswers", 
                "PollAnswers.questionID = PollQuestions.id");
        $this->db->where(array('PollAnswers.questionID' => $pollid));
        $this->db->order_by("PollAnswers.id", "asc");
        $rows = $this->db->get()->result();
        if (!$rows) {
            return false;
        }
        $list['id'] = $rows[0]->PollID;
        $list['title'] = $rows[0]->title;
        $list['question'] = $rows[0]->question;
        foreach ($rows as $num => $row) {
            $list['answers'][] = $row->Answer;
        }
        return $list;
    }

        /**
         * Gets all votes for a specifc poll/question combination
         * @param type $pollid
         * @return boolean or votes for a poll
         */
    public function getVotesQuestion($pollid) {
        $this->db->select('count(userAnswer) AS voteCount, '
                . 'UserVotes.questionID AS userVoteQID');
        $this->db->select('PollAnswers.id , PollAnswers.answerNumber, '
                . 'PollAnswers.questionID AS answerVoteQID');
        $this->db->from("PollAnswers");
        $this->db->join("UserVotes", 
                "PollAnswers.id = UserVotes.answerID", 'left');
        $this->db->where(array('PollAnswers.questionID' => $pollid));
        $this->db->group_by("PollAnswers.answerNumber");
        $rows = $this->db->get()->result();
        if (!$rows) {
            return false;
        }
        foreach ($rows as $num => $row) {
            $list['Votes'][$num] = intval($row->voteCount);
        }
        return $list;
    }

        /**
         * Inserts a vote into a given poll
         * First checks if the poll is valid before inserting
         * @param type $pollid
         * @param type $answer
         * @param type $ipAddress
         * @return boolean or good result on success
         */
    function updateVote($pollid, $answer, $ipAddress) {
        // check if valid answer  and if not return false
        $this->db->select('PollAnswers.answerNumber,PollAnswers.id');
        $this->db->from("PollAnswers");
        $this->db->where(array('PollAnswers.questionID' => $pollid));
        $this->db->where(array('PollAnswers.answerNumber' => $answer));
        $this->db->group_by("PollAnswers.answerNumber");
        $rows = $this->db->get()->result();
        $answerid = $rows[0]->id;
        if (!$rows) {
            return false;
        }
        $data = array(
            'userAnswer' => $answer,
            'questionID' => $pollid,
            'answerID' => $answerid,
            'ipNumber' => $ipAddress,
        );
        $result = $this->db->insert('UserVotes', $data);
        return $result;
    }

        /**
         * Inserts a new poll
         * @param type $title
         * @param type $question
         * @param type $answerlist
         * @return number of rows altered
         */
    function insertPoll($title, $question, $answerlist) {
        $pollData = array(
            'title' => $title,
            'question' => $question,
        );
        $result = $this->db->insert('PollQuestions', $pollData);
        $pollID = $this->db->insert_id();

        foreach ($answerlist as $num => $answer) {
            $AnswerData = array(
                'questionID' => $pollID,
                'answerNumber' => $num + 1,
                'Answer' => $answer);
            $this->db->insert('PollAnswers', $AnswerData);
        }
        return $pollID;
    }

        /**
         * Modifies an existing poll and first checks if the poll is valid
         * @param type $pollID
         * @param type $title
         * @param type $question
         * @param type $answerlist
         * @return boolean
         */
    function updatePoll($pollID, $title, $question, $answerlist) {
        $pollData = array(
            'id' => $pollID,
            'title' => $title,
            'question' => $question,
        );
        $this->db->where('id', $pollID);
        $result = $this->db->update('PollQuestions', $pollData);
        if (!$result) {
            return false;
        }
        foreach ($answerlist as $num => $answer) {
            $AnswerData = array(
                'questionID' => $pollID,
                'answerNumber' => $num + 1,
                'Answer' => $answer);
            $this->db->where('questionID', $pollID);
            $this->db->where('answerNumber', $num + 1);
            $result = $this->db->update('PollAnswers', $AnswerData);
        }
        return $result;
    }

        /**
         * Deletes a given poll
         * @param type $pollId
         * @return type
         */
    function deltePoll($pollId) {
        $this->db->delete('PollQuestions', array('id' => $pollId));
        $this->db->delete('UserVotes', array('questionID' => $pollId));
        $this->db->delete('PollAnswers', array('questionID' => $pollId));
        return $this->db->affected_rows();
    }

        /**
         * delete all votes on a specific poll
         * @param type $pollid
         * @return type
         */
    function deletePollVotes($pollid) {
        $this->db->where(array('UserVotes.questionID' => $pollid));
        $this->db->delete('UserVotes');
        return $this->db->affected_rows();
    }
}
