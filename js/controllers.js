/*
 * 
 * @author Jamie Sheriff
 * @category Angular Javascript controller
 */
(function () {
    'use strict';
    /*jslint browser: true, devel: true, indent: 4, maxlen: 80 */
    /*global $, angular, alert*/
    /* Controllers */

    // Global "database"
    var BASEURL = window.location.origin + window.location.pathname +
            "index.php/services/",
        pollControllers = angular.module('pollControllers', []);

    pollControllers.controller('PollCtrl', ['$scope', '$http', '$routeParams',
        function ($scope, $http) {
            var pollURL = BASEURL + "polls";
            $http.get(pollURL).success(function (response) {
                $scope.pollList = response;
            });
        }
        ]);

    pollControllers.controller('CreateCtrl', ['$scope', '$http', '$location',
        function ($scope, $http, $location) {
            // defualt count for answers is answer count
            $scope.answerCount = 3;
            var poll =
                    {
                        title: '',
                        question: '',
                        answers: ["", "", ""]
                    };
            $scope.newPoll = poll;
            //updates the answer list length by pushing or popping an item
            $scope.updateAnswerList = function (add) {
                var answerLen = $scope.newPoll.answers.length;
                if (add) {
                    $scope.newPoll.answers.push('');
                } else {
                    if (answerLen > 2) {
                        $scope.newPoll.answers.pop();
                    }
                }
            };
            //submits the create poll form, will display alert if fails
            $scope.submit = function () {
                if ($scope.newPoll) {
                    var pollURL = BASEURL + "polls";
                    $http({
                        method: 'POST',
                        url: pollURL,
                        headers: {'Content-Type': 'application/json'},
                        data: JSON.stringify($scope.newPoll)
                    }).success(function () {
                        alert('Poll has been created!');
                        $location.path('/polls');
                    }).error(function (data) {
                        alert(data.error);
                        $location.path('/polls');
                    });
                }
            };
        }
        ]);

    pollControllers.controller('modifyCtrl', ['$scope', '$http', '$routeParams',
        '$location',
        function ($scope, $http, $routeParams, $location) {
            var PollID = $routeParams.pollID,
                pollURL = BASEURL + "polls/" + PollID;
        //generate a poll object if successful
            $http.get(pollURL).success(function (response) {
                var poll =
                        {
                            title: response.title,
                            question: response.question,
                            answers: response.answers
                        };
                $scope.ModdedPoll = poll;

            }).error(function (data) {
                alert(data.error);
                $location.path('/polls');
            });
             //submits a modfied poll and will alert on success or fail
            $scope.submitPoll = function () {
                if ($scope.ModdedPoll) {
                    $http({
                        method: 'PUT',
                        url: pollURL,
                        headers: {'Content-Type': 'application/json'},
                        data: JSON.stringify($scope.ModdedPoll)
                    }).success(function () {
                        alert('Poll modifications have been submitted');
                        $location.path('/polls');
                    }).error(function (data) {
                        alert(data.error);
                    });
                }
            };
        }
        ]);

    pollControllers.controller('viewCtrl', ['$scope', '$http', '$routeParams',
        '$location', '$route',
        function ($scope, $http, $routeParams, $location, $route) {
            $scope.chartType = 'pie';
            $scope.showChart = false;
            var PollID = $routeParams.pollID,
                pollURL = BASEURL + "polls/" + PollID,
                voteURL = BASEURL + "votes/" + PollID,
                //generates a graph with a check if one answer has a vote
                generateGraph = function (poll, votes) {
                    var datalist = [],
                        length = poll.answers.length,
                        i;
                    for (i = 0; i < length; i += 1) {
                        if (votes.Votes[i] > 0 && !$scope.showChart) {
                            $scope.showChart = true;
                        }
                        datalist.push({
                            x: poll.answers[i],
                            y: [votes.Votes[i]],
                            tooltip: " Votes: " + [votes.Votes[i]] + ", " +
                                poll.answers[i]
                        });
                    }
                 //Only generate graph data if data exists
                    if ($scope.showChart) {
                        $scope.graphData = {
                            series: poll.answers,
                            data: datalist
                        };
                        $scope.graphConfig = {
                            labels: false,
                            title: "Poll Answers",
                            legend: {
                                display: true,
                                position: 'left'
                            },
                            innerRadius: 0
                        };
                    }
                };
            $http.get(pollURL).success(function (pollResponse) {
                $scope.ansList = pollResponse;
                $http.get(voteURL).success(function (voteResponse) {
                    $scope.voteList = voteResponse;
                    if (pollResponse.answers.length !==
                            voteResponse.Votes.length) {
                        alert("Bad data was recieved for the poll and votes");
                    }
                    generateGraph(pollResponse, voteResponse);
                });
            }).error(function (data) {
                alert(data.error);
                $location.path('/polls');
            });
            $scope.currentPollID = $routeParams.pollID;
            //clear all the votes on a given poll with an alert confirm prompt
            $scope.clearVotes = function (poll) {
                if (confirm('Are you sure you want to clear the poll ' + poll +
                        '?')) {
                    var delURL = BASEURL + "votes/" + PollID;
                    $http.delete(delURL).success(function () {
                        alert("Votes Cleared");
                        $route.reload();
                    }).error(function (data) {
                        alert(data.error);
                        $route.reload();
                    });
                }
            };
            //Delete a given poll with an alert confirm prompt
            $scope.deletePoll = function (poll) {
                if (confirm('Are you sure you want to delete the poll ' + poll +
                        '?')) {
                    var delURL = BASEURL + "polls/" + PollID;
                    $http.delete(delURL).success(function () {
                        alert('Poll has been deleted');
                        $location.path('/polls');
                    }).error(function (data) {
                        alert(data.error);
                        $location.path('/polls');
                    });
                }
            };
        }
        ]);

    pollControllers.controller('voteCtrl', ['$scope', '$http', '$routeParams',
        '$location',
        function ($scope, $http, $routeParams, $location) {
            var PollID = $routeParams.pollID,
                pollURL = BASEURL + "polls/" + PollID;
            $scope.currentPollID = $routeParams.pollID;
            $http.get(pollURL).success(function (response) {
                $scope.pollDetail = response;
            }).error(function (data) {
                alert(data.error);
                $location.path('/polls');
            });
            //Posts a vote with a success or fail alert
            $scope.submitForm = function () {
                var answer = $scope.answersID,
                    voteURL = BASEURL + "votes/" + PollID + '/' + answer;
                $http.post(voteURL).success(function () {
                    alert('vote recorded!');
                    $location.path('/polls');
                }).error(function (data) {
                    alert(data.error);
                    $location.path('/polls');
                });
            };
        }
        ]);
    //controller that handles nav tabs if they are active or not
    pollControllers.controller('NavCtrl', function ($scope, $location) {
        $scope.isActive = function (route) {
            return route === $location.path();
        };
    });
}());