/*
 * 
 * @author Jamie Sheriff
 * @category Angular App module
 */

(function () {
    'use strict';
    /*jslint browser: true, devel: true, indent: 4, maxlen: 80 */
    /*global angular*/

    /* App Module */
    var VoteApp = angular.module('VoteApp', [
        'ngRoute',
        'pollControllers',
        'angularCharts'
    ]);
    /* Route Config */
    VoteApp.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/home', {
                    templateUrl: 'partials/home.html'
                }).
                when('/polls', {
                    templateUrl: 'partials/polls.html',
                    controller: 'PollCtrl'
                }).
                when('/voting/:pollID', {
                    templateUrl: 'partials/voting.html',
                    controller: 'voteCtrl'
                }).
                when('/viewing/:pollID', {
                    templateUrl: 'partials/viewing.html',
                    controller: 'viewCtrl'
                }).
                when('/about', {
                    templateUrl: 'partials/about.html',
                    title: 'about'
                }).
                when('/create', {
                    templateUrl: 'partials/create.html',
                    controller: 'CreateCtrl'
                }).
                when('/modify/:pollID', {
                    templateUrl: 'partials/modify.html',
                    controller: 'modifyCtrl'
                }).
                when('/myCarousel', {
                    templateUrl: 'partials/home.html'
                }).
                otherwise({
                    redirectTo: '/home'
                });
        }]);
}());
